import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {

  },
  // ! mutations มีหน้าที่เปลื่ยนค่าใน state เท่านั้น
  mutations: {

  },
  //! Vue จะติดต่อ action ไม่ติดต่อ mutations ตรงๆ
  actions: {

  },
  modules: {
    auth
  }
})
