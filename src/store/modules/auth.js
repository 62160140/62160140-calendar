import { AUTH_LOGIN, AUTH_LOGOUT } from '../mutation-type'
import router from '../../router/index'

import { login } from '../../services/auth'
const moduleA = {

  namespaced: true,
  state: () => ({
    userLogin: JSON.parse(localStorage.getItem('user'))

  }),
  // ! mutations มีหน้าที่เปลื่ยนค่าใน state เท่านั้น
  mutations: {
    [AUTH_LOGIN] (state, payload) {
      state.userLogin = payload
    },
    [AUTH_LOGOUT] (state) {
      state.userLogin = null
    }
  },
  //! Vue จะติดต่อ action {dispatch()} ไม่ติดต่อ mutations {commit()} ตรงๆ
  actions: {
    // payload คือ object ที่ส่งเข้ามา
    async  login (context, payload) {
      try {
        const res = await login(payload.email, payload.password)
        const user = res.data.user
        //! save to local storage
        const token = res.data.token
        localStorage.setItem('token', token)
        localStorage.setItem('user', JSON.stringify(user))
        //! end save to local storage
        // if login correct
        // const user = { email: payload.email, password: payload.password }
        router.push('/')
        context.commit(AUTH_LOGIN, user)
      } catch (e) {
        console.log('Error')
      }
    },
    logout (context) {
      /* -------------------- clear storage ------------------- */
      localStorage.removeItem('token')
      localStorage.removeItem('user')
      context.commit(AUTH_LOGOUT)
    }
  },
  getters: {
    isLogin (state, getters) {
      return state.userLogin != null
    }
  }
}

export default moduleA
